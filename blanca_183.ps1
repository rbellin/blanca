﻿#########################
# Blanca
#
# written by BELLIN Raphael
#
# 2021 - v1.83
#########################

function Show-Menu {
    Clear-Host
    Write-Host "  ___  _       __    _  _  ____    __     "
    Write-Host " |   \| |     /  \  | \| |/ ___|  /  \    "
    Write-Host " | * /| |    / /\ \ |  \ | |     / /\ \   "
    Write-Host " |   \| |__ / ____ \| \  | |___ / ____ \  "
    Write-Host " |___/|____/_/    \_\_|\_|\____/_/    \_\ "
    Write-Host "=================== MENU ================="   
    Write-Host "1: Activate Windows.           6: Delete Unknown Accounts"
    Write-Host "2: Install Fusion Inventory.   7: WLAN dump"
    Write-Host "3: Update clock.               8: F-Secure"
    Write-Host "4: Disable FireWall            9: "
    Write-Host "5: Veyon VNC                   10: GPUpdate /force + WSUS"
    Write-Host ""
    Write-Host "Q: Press 'Q' to quit."
    Write-Host ""
    Write-Host "=============================================="
}

## 1
#Activate Windows
#
#slmgr activate a key with '/ipk' then use '/ato' check online if everything is correct
#both commands are mandatory
#'//b' is a quiet mode
#
#function ActivateWin {
#    Clear-Host
#    Write-Host "Activating Windows:"
#    $PassKey = [byte]00,00,00,00,00...
#
#    $password = $PSScriptRoot+"KEY"
#    $key = Get-Content $password | Convertto-SecureString -Key $PassKey
#            
#    Invoke-Command -ScriptBlock { slmgr //b /ipk $key }
#    Start-Sleep -s 20
#    Invoke-Command -ScriptBlock { slmgr //b /ato }
#
#    Write-Host "DONE !"
#}

function ActivateWin {
    Clear-Host
    (Get-WmiObject –query 'select * from SoftwareLicensingService').OA3xOriginalProductKey
}

## 2
#Install Fusion Inventory
#beware of your location, fusioninventory-agent_windows need to be in the same folder as this script
#
function InstallFusionInventory {
    Clear-Host
    
    $fi_wmi = Test-Path 'C:\Program Files\FusionInventory-Agent\fusioninventory-wmi.bat'
    $fi_agent = Test-Path 'C:\Program Files\FusionInventory-Agent\fusioninventory-agent.bat'

    if ($fi_agent -eq $false) {
        Write-Host "> installing Fusion Inventory"
        #./fusioninventory-agent_windows-x64_2.5.2.exe /S /acceptlicense /add-firewall-exception /execmode=service /runnow /httpd-trust='"127.0.0.1,localhost"' /scan-homedirs /scan-profiles /server='"http://10.10.0.233/plugins/fusioninventory/"'
        $Exe = $PSScriptRoot + '\fusioninventory-agent_windows-x64_2.5.2.exe'
        & $Exe /S /acceptlicense /add-firewall-exception /execmode=service /runnow /httpd-trust='"127.0.0.1,localhost"' /scan-homedirs /scan-profiles /server='"http://10.10.0.233/plugins/fusioninventory/"'
    
    }
    
    While ($fi_wmi -eq $false) {
        Start-Sleep -s 1
        $fi_wmi = Test-Path 'C:\Program Files\FusionInventory-Agent\fusioninventory-wmi.bat'
    }

    $fi_agent = Test-Path 'C:\Program Files\FusionInventory-Agent\perl\bin\fusioninventory-agent'
    if ($fi_agent -eq $true) {
        Write-Host "> starting fusioninventory-agent"
        Write-Host "> inventory in progress..."
        & 'C:\Program Files\FusionInventory-Agent\perl\bin\perl.exe' 'C:\Program Files\FusionInventory-Agent\perl\bin\fusioninventory-agent'
    } else {
        Write-Host "> fusioninventory-agent not found!"
    }
    Write-Host "DONE !" -ForegroundColor DarkGreen
}

## 3
#Update Clock
#add a reg key to swich on UTC time instead of localtime
#see https://wiki.archlinux.org/index.php/System_time#UTC_in_Microsoft_Windows
#
function UpdateClock {
    Clear-Host
    Write-Host "> updating Clock:"
    reg add "HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\TimeZoneInformation" /v RealTimeIsUniversal /d 1 /t REG_QWORD /f
    tzutil /s "Romance Standard Time"
    Write-Host "===="
    w32tm /tz
    Write-Host "DONE !" -ForegroundColor DarkGreen
}

## 4
#Disable Firewall
#we only need to disable the domain firewall, everything need to be kept
#
function DisableFirewall {
    Clear-Host
    Write-Host "> disabling firewall"
    Set-NetFirewallProfile -Profile Domain -Enabled False
    #Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled False 
    Write-Host "DONE !" -ForegroundColor DarkGreen
}

## 5
#Regedit VNC
#AF: Veyon
#
function Veyon_VNC
{
	Clear-Host
	reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run"  /V "Ultravnc" /t REG_SZ /F /D "C:\Program Files\UltraVNC\winvnc.exe"
	Write-Host "DONE !" -ForegroundColor DarkGreen
}

## 6
#Delete unknown accounts from the computer
#can be pretty long to do since the accounts often have more than 2GB of data to be deleted
function DeleteUnknownAccounts {
	Clear-Host
    Get-CimInstance win32_userprofile | foreach {
        ""
        $u = $_  # save our user to delete later 
        try {
            $objSID = New-Object System.Security.Principal.SecurityIdentifier($U.sid) -ErrorAction stop
            $objUser = $objSID.Translate( [System.Security.Principal.NTAccount])
            "User={0}" -f $objUser.Value
            "Path={0}" -f $u.LocalPath
            "SID={0}" -f $u.SID
        }
        catch {
            "!!!!!Account Unknown!!!!!"
            "Path={0}" -f $u.LocalPath
            "SID={0}" -f $u.SID
            Remove-CimInstance -inputobject $u -Verbose
        }
    }
}

## 7
#Dump WLAN info into a xml file and open it in defautl browser
#
# TODO: debug default browser + relative path
function WLAN_Dump {
	Clear-Host
    netsh wlan Show wlanreport
    Write-Host "/!\ WLAN folder moved to C:\ /!\"
    Write-Host ""
    Move-Item -Path C:\ProgramData\Microsoft\Windows\WlanReport -Destination C:\
    Write-Host "Opening WLAN dump in your default browser."
    Invoke-Item "C:\WlanReport\wlan-report-latest.html"
	Write-Host "DONE !" -ForegroundColor DarkGreen
}

## 8
#Update F-Secure

function FSecure {
	Clear-Host
    ## VARIABLES ====
    $valeur = (Get-ComputerInfo).OSProductType
    if ($valeur -ne "WorkStation") { Exit }
    $CP = '\fs_oneclient_logout.exe'
    $serie = "000-000-000-000-000”
    $fsecurecp = $PSScriptRoot + $CP
    ## INSTALL ====
    write-host "installation"
    $Command_line="$fsecurecp --psb1 --keycode $serie"
    write-host $Command_line
    iex $Command_line
    Start-Sleep -s 5
    New-Item -ItemType "file" -Path 'C:\ProgramData\F-Secure\PSB1.txt'
    
	Write-Host "DONE !" -ForegroundColor DarkGreen
}

## 10
#Update Windows
#gpupdate /force will download Group Policies from the server
#wuauctl.exe 
#
function GpupdateForce {
    Clear-Host
    Write-Host "> gpupdate /force"
    Invoke-Command -ScriptBlock { gpupdate /force }
    Write-Host "> Start scanning patches from the WSUS"
    Invoke-Command -ScriptBlock { usoclient StartScan }
    Write-Host "> Start downloading patches"
    Invoke-Command -ScriptBlock { usoclient StartDownload }

    Write-Host "DONE !" -ForegroundColor DarkGreen
}


## entry point
if($PSVersionTable.PSVersion.Major -lt 4) {
    $PSScriptRoot = $MyInvocation.MyCommand.Path | Split-Path
}

# Elevating privileges
function Test-Admin {
    $currentUser = New-Object Security.Principal.WindowsPrincipal $([Security.Principal.WindowsIdentity]::GetCurrent())
    $currentUser.IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)
}

if ((Test-Admin) -eq $false)  {
    if ($elevated) {
        # tried to elevate, did not work, aborting
    } else {
        Start-Process powershell.exe -Verb RunAs -ArgumentList ('-noprofile -noexit -file "{0}" -elevated' -f ($myinvocation.MyCommand.Definition))
        Set-ExecutionPolicy -Scope "CurrentUser" -ExecutionPolicy "Unrestricted"
    }
    exit
}

'running with full privileges'

#Authorize powershell 
#and set powershell location atthe right place instead of system32
Set-ExecutionPolicy -Scope "CurrentUser" -ExecutionPolicy "Unrestricted"
#Set-ExecutionPolicy -Scope Process -ExecutionPolicy Bypass 
Set-ExecutionPolicy RemoteSigned
#Set-Location -Path C:\blanca


#menu
do {
    Show-Menu
    $selection = Read-Host "Please make a selection"
		switch ($selection) 
		{
			'1' {
				ActivateWin
			} '2' {
				InstallFusionInventory
			} '3' {
				UpdateClock
			} '4' {
				DisableFirewall
			} '5' {
				Veyon_VNC
			} '6' {
				DeleteUnknownAccounts
			} '7' {
				WLAN_Dump
			} '8' {
				FSecure
			} '9' {
				#nothing
			} '10' {
				GPUpdateForce
			}
		}
    pause
	}
 until ($selection -eq 'q')
