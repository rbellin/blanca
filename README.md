# blanca

A utility tool in PowerShell has been created to assist with the deployment and ongoing maintenance of software at a company. This tool is designed to streamline and simplify the process of installing new software, as well as updating and managing existing software applications within the organization.

### Activate Windows
- commented version:
Translate the contents of a list of hexadecimal values into normal text, using a password that is stored in a separate file.
- enabled version:
Look at the original product key that came with your Windows operating system, and then proceed to use that key to activate the copy of Windows.
### InstallFusionInventory
Set up FusionInventory on the computer. 
After that, it makes sure the most recent file created during this setup is present and correct (fusioninventory-wmi.bat). I know it's not optimal but i did it quick at that time.
Once the file is written on the disk, it starts FusionInventory. It conducts a thorough check-up of every part of the computer.
When the inventory is complete, the software will send the details of all the computer components to the GLPI server.
### UpdateClock
Sysprep causes the computer's clock to stop functioning correctly.
To fix this problem, we update the clock settings by adding a new entry in the Windows registry. 
Specifically, we add a registry key that sets the time zone, then we force the computer to recognize and use the correct time zone with "Romance Standard Time."
### DisableFirewall
The firewall's domain does not work well with fusioninventory, since we do not have particular rules with the domain let's disable it.
### Veyon_VNC
Add a reg key for Veyon\_VNC.
### DeleteUnknownAccounts
Remove any user accounts that no longer exist from the computer, particularly if they are not part of the network domain.
### WLAN\_Dump
Dump WLAN info into a xml file and open it in browser.
### Update F-Secure
Upgrade the F-Secure antivirus program.
### GPUpdateForce
Update Windows
